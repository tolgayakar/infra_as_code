#!/usr/bin/env python3
import os

from aws_cdk import core

from web_app.web_app_stack import WebAppStack
import os


app = core.App()
account_id = os.environ["ACCOUNT_ID"]
env = core.Environment(account = account_id,region = "eu-west-2")
WebAppStack(app, "WebAppStack", env = env)

app.synth()
