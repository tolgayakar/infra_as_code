from aws_cdk import aws_ecr as ecr
from aws_cdk import core
def createRepositoryForImage(self):
    repository = ecr.Repository(
        self,
        "Repository",
        repository_name = "webapp"
        
    )
    
    core.CfnOutput(
        self,
        "servicearndeneme",
        value = repository.repository_uri
    )

    return repository.repository_uri