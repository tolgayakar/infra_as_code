from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_logs as logs
def createVPC(self):

   
    vpc = ec2.Vpc(
        self,
        "customVPCID",
        cidr = "10.10.0.0/16",
        max_azs = 2,
        nat_gateways = 1,
        subnet_configuration=[
            ec2.SubnetConfiguration(
                name="publicSubnet1", cidr_mask= 24,subnet_type  =ec2.SubnetType.PUBLIC
            ),
            ec2.SubnetConfiguration(
                name="privateSubnet1", cidr_mask= 24,subnet_type  =ec2.SubnetType.PRIVATE
            )
        ],
             
    )
    security_group = ec2.SecurityGroup(
        self,
        security_group_name = "CDK-Sec-Group",
        id = "CDK-security-group",
        vpc = vpc,
        allow_all_outbound = True,
        
    )
    security_group.add_ingress_rule(ec2.Peer.ipv4("0.0.0.0/0"),ec2.Port.tcp(22))
    security_group.add_ingress_rule(ec2.Peer.ipv4("0.0.0.0/0"),ec2.Port.tcp(443))
    security_group.add_ingress_rule(ec2.Peer.ipv4("0.0.0.0/0"),ec2.Port.tcp(80))


    return vpc

