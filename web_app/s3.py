from aws_cdk import aws_s3 as s3
from aws_cdk.core import RemovalPolicy
from aws_cdk import core
from aws_cdk import aws_iam as iam
from aws_cdk import aws_cloudfront as cloudfront
from aws_cdk import aws_cloudfront_origins as cloudfront_origins

def createS3(self):

    my_bucket = s3.Bucket(
        self,
        "BucketForImages",
        bucket_name = f"bucketforimagestohow12",
        versioned = False,
        public_read_access = False,
        encryption = s3.BucketEncryption.S3_MANAGED,
        removal_policy = core.RemovalPolicy.DESTROY                                                                  
    )


    my_bucket.add_to_resource_policy(
        iam.PolicyStatement(
        effect = iam.Effect.ALLOW,
        actions = ["s3:GetObject"],
        resources = [my_bucket.arn_for_objects("*")],
        principals = [iam.AnyPrincipal()] 
        )
    )

    cloudfront.Distribution(
        self,
        "myDist",
        default_behavior=cloudfront.BehaviorOptions(
            origin = cloudfront_origins.S3Origin(
            my_bucket
            )
        )
    )

    return my_bucket.bucket_name
