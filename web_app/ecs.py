from aws_cdk import aws_ecs as ecs
from aws_cdk import aws_iam as iam
from aws_cdk import aws_ec2 as ec2

def createECS(self,vpc,uri):
       
    cluster = ecs.Cluster(
        self,
        "WebCluster",
        vpc=vpc,
        cluster_name = "EcsCluster", 
    )

    # Add capacity to it
    cluster.add_capacity(
        "DefaultAutoScalingGroupCapacity",
        instance_type = ec2.InstanceType("t2.micro"),
        desired_capacity = 1
    )

    task_definition = ecs.Ec2TaskDefinition(
        self,
        "TaskDef",
        ""
    )

    task_definition.add_container(
        "DefaultContainer",
        image = ecs.ContainerImage.from_registry(f"{uri}"),
        memory_limit_mib = 512
    )

    # Instantiate an Amazon ECS Service
    ecs_service = ecs.Ec2Service(
        self,
        "Service",
        cluster = cluster,
        task_definition = task_definition,
        
    )