from aws_cdk import core as cdk
from constructs import Construct
from web_app.vpc import createVPC
from web_app.s3 import createS3
from web_app.sns import createSNS
from web_app.cloudtrail import createTrail
from web_app.ecr import createRepositoryForImage
from web_app.ecs import createECS
from web_app.iam_user import createUser
class WebAppStack(cdk.Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        


        client = "webapp"
        vpc = createVPC(self)
        bucket_name = createS3(self)
        alarm_topic = createSNS(self)
        createUser(self,bucket_name)
        createTrail(self,client,alarm_topic)
        uri = createRepositoryForImage(self)
        #createECS(self,vpc,uri)
